#!/bin/bash 
docker pull registry.gitlab.com/amer.hasanovic/fet_net
wget 'https://gitlab.com/amer.hasanovic/fet_base/raw/master/start_container' -O /usr/local/bin/start_container && chmod +x /usr/local/bin/start_container
wget 'https://gitlab.com/amer.hasanovic/fet_base/raw/master/bin/fzy?inline=false-o' -O /usr/local/bin/fzy && chmod +x /usr/local/bin/fzy
##########################################################################################################################################
cat >> ~/.bashrc << END
if [ -d "/opt1/" ]; then
  export BOOST_ROOT=/opt1/boost1.70
  export TERM=xterm-256color
  source /opt1/script.sh
  [ -z "\$TMUX" ] && exec tmux
fi

open_cloonix_pcap() {
  PCAP=\$(find /opt1/cloonix_data -name "*.pcap" | fzy)
  wireshark-gtk -k -i <(tail -f -c +0 "\$PCAP" )
}
END

source ~/.bashrc

